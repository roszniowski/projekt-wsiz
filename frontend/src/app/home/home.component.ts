import {Component, OnInit, Output} from '@angular/core';
import 'hammerjs';
import {HomeService} from "../home.service";
import {FormControl, FormGroup} from "@angular/forms";
import * as moment from 'moment';
import {MatSnackBar} from "@angular/material";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    private url = 'http://127.0.0.1:8000/';
    users = [];
    ticketForm = new FormGroup({
        reporter: new FormControl(''),
        type: new FormControl(''),
        area: new FormControl(''),
        priority: new FormControl(''),
        description: new FormControl('')
    });
    status = new FormControl('');
    public config = {
        area: '',
        description: '',
        priority: '',
        reporter: '',
        type: ''
    };
    public recentTicket = {
        id: '',
        area: '',
        description: '',
        priority: '',
        reporter: '',
        type: '',
        status: '',
        createdAt: ''
    };
    public ticketList = [];


    constructor(private homeService: HomeService, private _snackBar: MatSnackBar) {
    }

    ngOnInit() {
        this.index();
        this.getList();
    }

    index() {
        this.homeService.index()
            .subscribe(
                data => {
                    if (!data) return;
                    this.users = data['users'];
                },
                error => {
                    console.log('Error', error);
                });
    }

    onSubmit() {
        if (this.config !== this.ticketForm.value) {
            this.config = this.ticketForm.value;
            this.homeService.create(this.config)
                .subscribe(
                    data => {
                        if (!data) return;
                        this.getLast();
                    },
                    error => {
                        console.log('Error', error);
                    });
        } else {
            this.openSnackBar('Takie zadanie juz istnieje. Wprowadź inne dane.', '');
        }
    }

    getLast() {
        this.homeService.getLast()
            .subscribe(
                data => {
                    if (!data) return;
                    this.recentTicket = data['data'];
                    this.getList();
                },
                error => {
                    console.log('Error', error);
                });
    }

    getList() {
        this.homeService.getList()
            .subscribe(
                data => {
                    if (!data) return;
                    this.ticketList = data['data'];
                },
                error => {
                    console.log('Error', error);
                });
    }

    showTicket(ticket) {
        this.recentTicket = ticket;
    }

    formatDate(date) {
        return moment(date).format('Y-MM-DD HH:mm');
    }

    openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 2000,
        });
    }
}
