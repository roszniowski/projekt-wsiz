import {Injectable} from '@angular/core';
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {Ticket} from "./ticket";

@Injectable({
    providedIn: 'root'
})
export class HomeService {

    private url = 'https://127.0.0.1:8000';

    constructor(private http: HttpClient) {
    }

    index() {
        return this.http.get(this.url).pipe(
            catchError(error => {
                    return throwError(error.message || 'Server error');
                }
            ));
    }

    create(config): Observable<any> {
        return this.http.post<any>(this.url + '/create', config).pipe(
            catchError(error => {
                    return throwError(error.message || 'Server error');
                }
            ));
    }

    getLast() {
        return this.http.get(this.url + '/get_last').pipe(
            catchError(error => {
                    return throwError(error.message || 'Server error');
                }
            ));
    }

    getList() {
        return this.http.get(this.url + '/get_list').pipe(
            catchError(error => {
                    return throwError(error.message || 'Server error');
                }
            ));
    }
}
