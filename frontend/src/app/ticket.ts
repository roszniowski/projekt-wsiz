export class Ticket {
    constructor(
        public reporter: number,
        public area: string,
        public priority: string,
        public description: string
    ) {  }
}
