<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191203194650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('CREATE TABLE role (id SERIAL NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE users (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, role INT NOT NULL REFERENCES public.role(id), PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ticket (id SERIAL NOT NULL, reporter_id INT NOT NULL REFERENCES public.users(id), type VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, area VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, priority VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO public.role values (1, \'dev\')');
        $this->addSql('INSERT INTO public.role values (2, \'user\')');
        $this->addSql('INSERT INTO public.users values (1, \'Radek Roszniowski\', 1)');
        $this->addSql('INSERT INTO public.users values (2, \'Kamila Gontarz\', 1)');
        $this->addSql('INSERT INTO public.users values (3, \'Tomasz Korczak\', 1)');
        $this->addSql('INSERT INTO public.users values (4, \'Student\', 2)');
    }

    public function down(Schema $schema) : void
    {
    }
}
