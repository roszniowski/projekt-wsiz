<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191204222636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ticket DROP CONSTRAINT ticket_reporter_id_fkey');
        $this->addSql('ALTER TABLE ticket RENAME COLUMN reporter_id TO reporter');
        $this->addSql('ALTER TABLE users DROP CONSTRAINT users_role_fkey');
    }

    public function down(Schema $schema) : void
    {
    }
}
