<?php

namespace App\Repository;

use App\Entity\Ticket;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    public function createNewTicket($params, $entityManager){
        $date = new DateTime('now');
        $ticket = new Ticket();
        $ticket->setReporter($params['reporter'] ?? 'Radek Roszniowski');
        $ticket->setCreatedAt($date->format('Y-m-d H:m:s'));
        $ticket->setArea($params['area'] ?? '');
        $ticket->setPriority($params['priority'] ?? '');
        $ticket->setStatus('NEW');
        $ticket->setType($params['type'] ?? '');
        $ticket->setDescription($params['description'] ?? '');
        $entityManager->persist($ticket);
        $entityManager->flush();
    }

    public function getLastlyCreated(){
        $qb = $this->createQueryBuilder('p')
            ->setMaxResults( 1 )
            ->orderBy('p.id', 'DESC');
        return $qb->getQuery()->getSingleResult();
    }

    public function getList()
    {
        return $this->findBy([], ['id' => 'DESC']);
    }
}
