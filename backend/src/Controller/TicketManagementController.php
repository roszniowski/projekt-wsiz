<?php


namespace App\Controller;

use App\Repository\TicketRepository;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class TicketManagementController extends AbstractController
{
    public function indexAction(UsersRepository $userRepository)
    {
        return $this->json([
            'message' => 'success',
            'users' => $userRepository->findAll()
        ], 200);
    }

    public function createAction(Request $request, TicketRepository $ticketRepository)
    {
        $params = json_decode($request->getContent(), true);
        $ticketRepository->createNewTicket($params, $this->getDoctrine()->getManager());
        return $this->json([
            'message' => 'New entry has been added.'
        ], 200);
    }

    public function getLastAction(TicketRepository $ticketRepository)
    {
        $last = $ticketRepository->getLastlyCreated();
        return $this->json([
            'data' => $last
        ], 200);
    }

    public function getTicketList(TicketRepository $ticketRepository)
    {
        $data = $ticketRepository->getList();
        return $this->json([
            'data' => $data
        ], 200);
    }
}
